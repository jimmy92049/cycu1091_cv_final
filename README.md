# CYCU1091_CV_FINAL
## 說明
1. FINAL-PROJECT/ 為離線版網頁，僅供離線測試使用，點開index.html後，等待左上角的提示訊息消失即可開始操作。
2. cvFinal/ 為主要文件資料夾，包含flaskFinal.py主要程式，執行即可啟用flask web server。
3. 三個yaml檔案為k8s部署用的配置文件。

## 遊戲操作
1. 等待左上角Loading模型的訊息消失，表示載入完成。
2. 點擊"Open the camera"按鈕，開啟鏡頭。
3. 點擊"Add"按鈕拍照加入辨識分類，可以用喜歡的表情自訂分類。
+ IDLE： 表示靜止狀態，或不操作。
+ JUMP： 表示跳躍的動作。
+ DUCK： 蹲下/低頭/躲。
+ Reset Settings： 重置分類。
4. 滑鼠點擊一下人物，按"空白鍵"開始遊戲。
 
